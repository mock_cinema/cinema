import actBanking from "../actionCenima/actBanking";

const initialState = {
    lsBank:[],

}

const rdcBanking = (state = initialState, {type, payload}) => {
    switch (type) {
        case actBanking.SET_BANK:
            return {
                ...state,
                lsBank:payload,
            }
            
        default:
            return state;
    }
}

export default rdcBanking
