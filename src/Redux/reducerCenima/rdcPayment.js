import actPayment from "../actionCenima/actPayment";

const initialState = {
    lsPayment:[],
    isSuccess: null,
    lsTicket:[]
}

const rdcPayment = (state = initialState, {type, payload}) => {
    switch (type) {
        case actPayment.SET_PAYMENT:
            return {
                ...state,
                lsPayment:payload,
            }
        case actPayment.SUCCESS_PAYMENT:
            return {
                ...state,
                isSuccess: true,
            }
        case actPayment.FAILED_PAYMENT:
            return {
                ...state,
                isSuccess: false,
            }
        case actPayment.SET_TICKET:
            return {
                ...state,
                lsTicket: payload,
            }
            
        default:
            return state;
    }
}

export default rdcPayment
