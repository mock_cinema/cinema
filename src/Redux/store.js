import { applyMiddleware, combineReducers, createStore } from "redux";
import rdcCenima from "./reducerCenima/rdcCenima";
import createSaga from "redux-saga";
import middleSaga from "./saga/middleSaga";
import rdcSeat from "./reducerCenima/rdcSeat";
import rdcBanking from "./reducerCenima/rdcBanking";
import rdcPayment from "./reducerCenima/rdcPayment";

const saga = createSaga()
const globalState = combineReducers({
    dataManage: rdcCenima,
    seatManage: rdcSeat,
    bankManage: rdcBanking,
    paymentManage: rdcPayment
})
const store = createStore(
    globalState,
    applyMiddleware(saga)
)
export default store;
saga.run(middleSaga)