import React from 'react'
import actPayment from '../../Redux/actionCenima/actPayment'
import { connect } from 'react-redux'
import './History.scss'

function History() {
  return (
    // <div className='History'>
    //     <table>
    //         <thead>
    //             <tr>
    //                 <th>Ngày</th>
    //                 <th>Số giao dịch</th>
    //                 <th>Mã vé</th>
    //                 <th>Rạp</th>
    //                 <th>Phim</th>
    //                 <th>Chú thích</th>
    //                 <th>Giá trị</th>
    //                 <th>Sao tích lũy</th>
    //                 <th>Điểm đã dùng</th>
    //                 <th>QR code</th>
    //                 </tr>
    //         </thead>
    //     </table>
    // </div>
    <div className='SelectionTicket'>
            <div className='Container'>
                <div className='selectionTicket'>
                    <h1>LỊCH SỬ GIAO DỊCH</h1>
                    <div className='loaive'>
                        <table>
                            <thead>
                                <tr>
                                    <th>Loại vé</th>
                                    <th>Số lượng </th>
                                    <th>Giá(VNĐ)</th>
                                    <th>Tổng(VNĐ)</th>
                                </tr>
                            </thead>
                            <tbody className='ticketType'>
                                {/* {
                                    props.dataCenima.lsBookingDetail.ticket?.map((item, index) => {
                                        return (
                                            <CartItem
                                                key={index}
                                                item={item}
                                                quantity={cartItemsTicket[index] || 0}
                                                onChange={(quantity) => handleQuantityChangeTicket(index, quantity)}
                                            />
                                        )
                                    })
                                } */}
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
  )
}

const mapStateToProps = (globalState) => {
    return {
        dataTicket: globalState.paymentManage
    }
}
const mapDispatchToProps = (dispath) => {
    return {
        GetTicket:() => {
          dispath({
            type:actPayment.GET_TICKET
          })
        }

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(History);