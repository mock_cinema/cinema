import React from 'react';

const Loading = () => {
  return (
    <div className="loading-container">
      <img src='../img/loading.gif' alt="Loading..." className="loading-icon" />
    </div>
  );
};

export default Loading;
