import React from 'react'

export default function CartItem({ item, quantity, onChange }) {
    const handleQuantityChange = (e) => {
        const newQuantity = parseInt(e.target.value);
          onChange( newQuantity);
      };

      const calculateTotalPrice = (item, price) => {
        return price * item;
      };                                      
    
  return (
    <>
        {/* <tr>
            <td>{item.name || item.description}</td>
            <td>
                <input
                    type="number"
                    value={quantity}
                    onChange={handleQuantityChange}
                />
            </td>
            <td className='prices'>{item.displayPrice.toLocaleString()}</td>
        </tr>  */}

        <tr>
            <td>
            <p>{item.name}</p>
            <p>{item.description}</p>
            <p>{item.extendedDescription}</p>
            </td>

            <td>
            <button style={{ padding: "5px", fontWeight: "bold", margin: "5px" }}>-</button>
            <input
            style={{ width: "40px", textAlign: "center" }}
            type='number'
            value={quantity}
            onChange={handleQuantityChange}
            />
            <button style={{ padding: "5px", fontWeight: "bold", margin: "5px" }}>+</button>
            </td>

            <td>{item.displayPrice.toLocaleString()}</td>
            <td>{calculateTotalPrice(quantity, item.displayPrice).toLocaleString()}</td>
        </tr>
    </>
  )
}

