import React, { useEffect, useState } from 'react'
import "./SelectionTicket.scss"
import { connect } from "react-redux";
import actCenima from '../../../Redux/actionCenima/actCenima';
import CartItem from './CartItem';
import { useNavigate } from 'react-router-dom';
import Popup from '../../Popup/Popup';

function SelectionTicket(props) {

    const nav = useNavigate()
    const [cartItemsTicket, setCartItemsTicket] = useState({});
    const [cartItemsCombo, setCartItemsCombo] = useState({});
    const [showPopup, setShowPopup] = useState(false);

    let totalSum = 0;
    let ticketCount = 0;
    let comboCount = 0;
    let ticketCouple = 0;
    let ticketSingle = 0;
    let combo = ""

    const ticket = JSON.parse(sessionStorage.getItem("Ticket"));

        useEffect(() => {
        props.GetBookingDetail()
        //props.GetBooking()
    }, [])


    //const movie = props.dataCenima.lsBooking.movies?.find(n => n.id == idMovieTicket)

    const handleQuantityChangeTicket = (index, quantity) => {
        if (quantity >= 0 && quantity <= 100) {
          setCartItemsTicket((prevItems) => ({ ...prevItems, [index]: quantity }));
        }
      };
    
      const handleQuantityChangeCombo = (index, quantity) => {
        if (quantity >= 0 && quantity <= 100) {
          setCartItemsCombo((prevItems) => ({ ...prevItems, [index]: quantity }));
        }
      };
    
      const calculateTotalPrice = (item, price) => {
        return price * item;
      };                                      
    
      const handleClearCart = () => {
        setCartItemsTicket({});
        setCartItemsCombo({});
      };
     
      const Confirm = (totalSum,ticketCount,comboCount,ticketCouple,ticketSingle,combo) => {
       
        if (ticketCount === 0) {
            // Clear the sessionStorage when both ticketCount and comboCount are 0
          //alert('Vui lòng chọn số lượng vé')
          setShowPopup(true);
        

          } else {
            const Ticket = {
              ...ticket,
              totalSum: totalSum,
              ticketCount: ticketCount,
              comboCount: comboCount,
              ticketCouple: ticketCouple,
              ticketSingle: ticketSingle,
              comboName: combo
            };
            setShowPopup(false);
            sessionStorage.setItem('Ticket', JSON.stringify(Ticket));
            nav('/selectionseat'); // Navigate to the desired page
          }
    }

    const handleClosePopup = () => {
        setShowPopup(false);
      };

     


     
    // const Up = (TypeCode) => {
    //     // const updateLsTicket = lsTicket.map((n) => {
    //     //     if (n.ticketTypeCode === TypeCode) {
    //     //         return {
    //     //             ...n,
    //     //             defaultQuantity: n.defaultQuantity * 1 + 1
    //     //         };
    //     //     }
    //     //     return n
    //     // })
    //     // setLsTicket(updateLsTicket)
    // }

    // const Down = (TypeCode) => {
    //     // const updateLsTicket = lsTicket.map((n) => {
    //     //     if (n.ticketTypeCode === TypeCode && n.defaultQuantity > 0) {
    //     //         return {
    //     //             ...n,
    //     //             defaultQuantity: n.defaultQuantity * 1 - 1
    //     //         };
    //     //     }
    //     //     return n
    //     // })
    //     // setLsTicket(updateLsTicket)
    // }
    return (
        
        <div className='SelectionTicket'>
            <div className='Container'>
                <div className='selectionTicket'>
                    <h1>CHỌN VÉ/THỨC ĂN</h1>
                    <div className='loaive'>
                        <table>
                            <thead>
                                <tr>
                                    <th>Loại vé</th>
                                    <th>Số lượng </th>
                                    <th>Giá(VNĐ)</th>
                                    <th>Tổng(VNĐ)</th>
                                </tr>
                            </thead>
                            <tbody className='ticketType'>
                                {
                                    props.dataCenima.lsBookingDetail.ticket?.map((item, index) => {
                                        return (
                                            <CartItem
                                                key={index}
                                                item={item}
                                                quantity={cartItemsTicket[index] || 0}
                                                onChange={(quantity) => handleQuantityChangeTicket(index, quantity)}
                                            />
                                        )
                                    })
                                }
                            </tbody>

                            <thead>
                                <tr>
                                    <th>Combo</th>
                                    <th>Số lượng </th>
                                    <th>Giá(VNĐ)</th>
                                    <th>Tổng(VNĐ)</th>
                                </tr>
                            </thead>
                            <tbody className='Combo'>
                                {
                                    props.dataCenima.lsBookingDetailCb?.map((item, index) => {
                                        return (
                                            <CartItem
                                                key={index}
                                                item={item}
                                                quantity={cartItemsCombo[index] || 0}
                                                onChange={(quantity) => handleQuantityChangeCombo(index, quantity)}
                                            />
                                        )
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
                
                    {/* movie ?  */}
                    <div className='infoTicket'>
                        <div>
                            <img src={ticket.imgMovie} />
                        </div>
                        <div>
                            <h2 className='name'>{ticket.nameMovie}</h2>
                        </div>
                        <div>
                            <p><span>Rạp: </span>{ticket.CinemaName}</p>
                            <p><span>Screen:</span>{ticket.ScreenName}</p>
                            <p><span>Suất chiếu: </span>{ticket.Showtime} | {ticket.DateShow}, {ticket.DayOfWeek}</p>
                            <p><span>Vé: </span>
                            {
                                props.dataCenima.lsBookingDetail.ticket?.map((item, index) => {
                                    const quantity = cartItemsTicket[index] || 0;
                                    if (quantity > 0) {
                                    totalSum += calculateTotalPrice(quantity, item.displayPrice);
                                    ticketCount += quantity
                                    if (index === 2) {
                                        ticketCouple += quantity;
                                    } if (index === 1 || index === 0){
                                        ticketSingle += quantity
                                    }
                                    return (
                                        <li key={index}>
                                        {quantity} x {item.name}
                                        {/* : {calculateTotalPrice(quantity, item.displayPrice).toLocaleString()} */}
                                        </li>
                                    )
                                    } 
                                    return null;
                                })
                            }
                            </p>
                            <p><span>Combo: </span>
                            {
                                props.dataCenima.lsBookingDetailCb?.map((item, index) => {
                                    const quantity = cartItemsCombo[index] || 0;
                                    if (quantity > 0) {
                                    totalSum += calculateTotalPrice(quantity, item.displayPrice);
                                    comboCount += quantity
                                    combo += item.extendedDescription + ', '
                                    return (
                                        <li key={index}>
                                        {quantity} x {item.description}<br/>({item.extendedDescription})
                                        {/* : {calculateTotalPrice(quantity, item.displayPrice).toLocaleString()} */}
                                        </li>
                                    )
                                    } console.log(combo)
                                    return null;
                                })
                            }
                            </p>
                            <p><span>Tổng: </span>{totalSum.toLocaleString()}</p>
                        </div>
                        <div className='bnt'>
                            <button onClick={() => Confirm(totalSum,ticketCount,comboCount,ticketCouple,ticketSingle,combo)}>Tiếp Tục</button>
                        </div>
                        {showPopup && <Popup noti="Vui lòng chọn số lượng vé" onClose={handleClosePopup} />}
                    </div> 
            </div>
        </div>
    )
}
const mapStateToProps = (globalState) => {
    return {
        dataCenima: globalState.dataManage
    }
}
const mapDispatchToProps = (dispath) => {
    return {
        GetBookingDetail: () => {
            dispath({
                type: actCenima.GET_BOOKING_DETAIL
            })
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SelectionTicket);
