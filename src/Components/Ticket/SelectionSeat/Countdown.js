import React, { useEffect, useState } from 'react';

const Countdown = ({ minutes, onTimeout }) => {
  const initialTime = minutes * 60;
  const [time, setTime] = useState(sessionStorage.getItem('countdownTime') || initialTime);

  useEffect(() => {
    let intervalId;

    const tick = () => {
      setTime((prevTime) => {
        const newTime = prevTime - 1;
        sessionStorage.setItem('countdownTime', newTime); // Save the updated time to localStorage
        return newTime;
      });
    };

    // Start the interval when the component mounts
    intervalId = setInterval(tick, 1000);

    // Cleanup function to clear the interval when the component unmounts
    return () => {
      clearInterval(intervalId);
    };
  }, []);

  useEffect(() => {
    // Check if the time has reached zero
    if (time <= 0) {
      onTimeout();
    }
  }, [time, onTimeout]);

  const formatTime = (timeInSeconds) => {
    const minutes = Math.floor(timeInSeconds / 60);
    const seconds = timeInSeconds % 60;
    return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
  };

  return (
    <div>
      <h1>Thời gian còn lại: {formatTime(time)}</h1>
    </div>
  );
};

export default Countdown;
