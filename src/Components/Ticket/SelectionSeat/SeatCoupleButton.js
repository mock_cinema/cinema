import React, { useState } from 'react'

export default function SeatCoupleButton({ seatName, isShow, seatPairs, seatCodeSelect }) {
    const [seatStates, setSeatStates] = useState(seatPairs.map(() => ({ isHovered: false, isChecked: false })));
    

    const handleMouseEnter = (index) => {
      setSeatStates((prevSeatStates) => {
        const updatedSeatStates = [...prevSeatStates];
        updatedSeatStates[index] = { ...updatedSeatStates[index], isHovered: true };
        return updatedSeatStates;
      });
    };
  
    const handleMouseLeave = (index) => {
      setSeatStates((prevSeatStates) => {
        const updatedSeatStates = [...prevSeatStates];
        updatedSeatStates[index] = { ...updatedSeatStates[index], isHovered: false };
        return updatedSeatStates;
      });
    };

    const handleClick = (index) => {
      setSeatStates((prevSeatStates) => {
        const updatedSeatStates = [...prevSeatStates];
        updatedSeatStates[index] = { ...updatedSeatStates[index], isChecked: !updatedSeatStates[index].isChecked };
        return updatedSeatStates;
      });
    
      const selectedSeats = JSON.parse(sessionStorage.getItem("selectedSeats")) || [];
      const selectedSeatCodes = seatPairs[index].map(n => `${seatName}${n}`);
    
      if (selectedSeatCodes) {
        // Check if all seat codes of this pair are already selected
        const allSelected = selectedSeatCodes.every(code => selectedSeats.includes(code));
    
        if (allSelected) {
          // All seats in this pair are already selected, remove them from the session
          const updatedSeats = selectedSeats.filter(code => !selectedSeatCodes.includes(code));
          sessionStorage.setItem("selectedSeats", JSON.stringify(updatedSeats));
          seatCodeSelect(updatedSeats);
        } else {
          // Some seats in this pair are not selected, add them to the session
          const updatedSeats = [...selectedSeats, ...selectedSeatCodes];
          sessionStorage.setItem("selectedSeats", JSON.stringify(updatedSeats));
          seatCodeSelect(updatedSeats);
        }
    
        // Update selectedSeatsData directly
        const selectedSeatsData = JSON.parse(sessionStorage.getItem("selectedSeatsData")) || { mode: 1, seats: [] };
    
        // Check if the seat codes of this pair are already in selectedSeatsData
        const seatCodesInData = selectedSeatCodes.every(code => selectedSeatsData.seats.some(seatData => seatData.seatCode === code));
    
        if (seatCodesInData) {
          // All seats in this pair are already in selectedSeatsData, remove them from the data
          const updatedSeatsData = {
            ...selectedSeatsData,
            seats: selectedSeatsData.seats.filter(seatData => !selectedSeatCodes.includes(seatData.seatCode)),
          };
          sessionStorage.setItem("selectedSeatsData", JSON.stringify(updatedSeatsData));
        } else {
          // Some seats in this pair are not in selectedSeatsData, add them to the data
          const updatedSeatsData = {
            ...selectedSeatsData,
            seats: [...selectedSeatsData.seats, ...selectedSeatCodes.map(seatCode => ({ mode: 2, seatCode }))],
          };
          sessionStorage.setItem("selectedSeatsData", JSON.stringify(updatedSeatsData));
        }
      }
    };
    
  
    const getBackgroundColor = (index) => {
      const seatState = seatStates[index];
      if (seatState && seatState.isChecked) {
        return 'blue'; // Change to your desired checked color
      } else if (seatState && seatState.isHovered) {
        return '#006a6a'; // Change to your desired hover color
      } else {
        return '#009d96'; // Change to your default color
      }
    };
  
    return (
      <div>
        {seatPairs.map((pair, index) => (
          <div
            key={index}
            style={{ display: 'inline-block', alignItems: 'left', margin: '5px', backgroundColor: getBackgroundColor(index) }}
          >
            <button
              onClick={() => handleClick(index)}
              style={{
                width: '25px',
                backgroundColor: getBackgroundColor(index),
                textAlign: 'center',
                color: 'white',
                borderRight:'1px solid white',
                fontSize:'11px'
              }}
              onMouseEnter={() => handleMouseEnter(index)}
              onMouseLeave={() => handleMouseLeave(index)}
            >
              {seatName}{pair[0]}
            </button>
            <button
              onClick={() => handleClick(index)}
              style={{
                width: '25px',
                backgroundColor: getBackgroundColor(index),
                textAlign: 'center',
                color:'white',
                fontSize:'11px'
              }}
              onMouseEnter={() => handleMouseEnter(index)}
              onMouseLeave={() => handleMouseLeave(index)}
            >
              {seatName}{pair[1]}
            </button>
          </div>
        ))} 
      </div>
    );
}
