import React from 'react'
import './SelectionSeat.scss'
import { connect } from 'react-redux';
import actSeat from '../../../Redux/actionCenima/actSeat';
import { useEffect } from 'react';
import { useState } from 'react';
import SeatButton from './SeatButton';
import SeatCoupleButton from './SeatCoupleButton';
import { useNavigate } from 'react-router-dom';
import Countdown from './Countdown';
import Popup from '../../Popup/Popup';


function SelectionSeat(props) {

  let ticket = JSON.parse(sessionStorage.getItem("Ticket"));

    //const infoOrder = sessionStorage.getItem("selectedSeats")
    //const order = JSON.parse(infoOrder);
  const nav = useNavigate()
  
  const [showPopup, setShowPopup] = useState(false);
      const handleClosePopup = () => {
        setShowPopup(false);
      };
        
    //const selectedSeatsData = JSON.parse(sessionStorage.getItem("selectedSeatsData")) || { mode: 1, seats: [] };
    const selectedSeats = JSON.parse(sessionStorage.getItem("selectedSeats")) || {};
    
    const [seatPairs, setSeatPairs] = useState([]);
    const [seatSelect, setSeatSelect] = useState([])

    useEffect(() => {
        props.GetSeat()
        props.GetSold()
      }, []);

      const handleSeatCode = (updatedSeats) => {
         setSeatSelect(updatedSeats);
      };
      
      useEffect(() => {
        const newSeatPairs = [];
        
        if (props.dataSeat.lsSeat1.rows) {
            
            props.dataSeat.lsSeat1.rows.map((row, rowIndex) => (
            row.seats.map((seat) => {
              if (seat.seatsInGroup) {
                const pair = seat.seatsInGroup.map((groupSeat) => groupSeat.columnIndex);
                newSeatPairs.push(pair);
              }
            }))); 
        }   
        // Remove duplicates from the seatPairs list
        const uniqueSeatPairs = newSeatPairs.filter((pair, index, self) => {
          return index === self.findIndex((p) => p[0] === pair[0] && p[1] === pair[1]);
        });
        
        setSeatPairs(uniqueSeatPairs);
        //console.log(newSeatPairs)
      }, [props.dataSeat.lsSeat1]);

      const checkOrder = (ticketSingle, ticketCouple) => {
        const selectedSeatsData = JSON.parse(sessionStorage.getItem("selectedSeatsData")) || { seats: [] };
        //const ticket = JSON.parse(sessionStorage.getItem("Ticket"));
        const seatsByMode = selectedSeatsData.seats.reduce((count, seat) => {
          count[seat.mode] = (count[seat.mode] || 0) + 1;
          return count;
        }, {});
      
        const mode1SeatsCount = seatsByMode[1] || 0;
        const mode2SeatsCount = seatsByMode[2] || 0;
      
        if (ticketSingle !== mode1SeatsCount || ticketCouple * 2 !== mode2SeatsCount) {
          //alert('Please check your ticket selection.');
          setShowPopup(true)
        } else {
          
          const official = {
            ...ticket,
            seatCode: Object.values(selectedSeats).join(", ")
          }

          sessionStorage.setItem('Ticket', JSON.stringify(official));
          nav('/payment')
        }
      
      };

      const handlePageUnload = () => {
        sessionStorage.removeItem('selectedSeats'); 
        sessionStorage.removeItem('selectedSeatsData'); 
      };

      useEffect(() => {
        // Attach the handlePageUnload function to the window.onbeforeunload event
        window.onbeforeunload = handlePageUnload;
    
        // Clean up the event listener when the component is unmounted
        return () => {
          window.onbeforeunload = null;
        };
      }, []);

      const onTimeout = () => {
        // Implement your logic when the countdown reaches zero
        nav('/')
        sessionStorage.removeItem('selectedSeats'); 
        sessionStorage.removeItem('selectedSeatsData');
        sessionStorage.removeItem('Ticket'); 
        localStorage.removeItem('countdownTime');
      };

     

  return (
    <div className='SelectionTicket'>
        <div className='Container'>
            <div className='selectionTicket'>
                <div style={{display:'flex', justifyContent:'space-between'}}><h1>CHỌN GHẾ</h1><Countdown minutes={1} onTimeout={onTimeout}/></div>
                <div className='loaive'>
                    <div>
                        {props.dataSeat.lsSeat.rows &&
                            props.dataSeat.lsSeat.rows.map((row, i) => (
                            <div key={i} style={{ display: 'flex', gap: '5px', marginBottom: '5px', justifyContent: 'center' }}>
                                <span style={{ width: '20px', visibility: row.seats.length === 0 ? 'hidden' : 'visible', color: 'black' }}>
                                {row.physicalName}
                                </span>
                                {Array(props.dataSeat.lsSeat.width).fill(0).map((_, i2) => {
                                const isShow = row.seats.some(s => s.position.rowIndex === i && s.position.columnIndex === i2);
                                const seatCode = `${row.physicalName}${i2}`;
                                //const isSold = seatCode.split(',').some((code) => props.dataSeat.lsSold.some((seat) => seat.SeatCode === code && !seat.isSold));
                                //const isSold = props.dataSeat.lsSold.some((seat) => seat.SeatCode === seatCode && !seat.isSold);
                                return <SeatButton key={i2} 
                                seatCode={seatCode} 
                                isShow={isShow} 
                                seatCodeSelect={handleSeatCode} 
                              />
                                })}
                               
                            </div>
                            ))}
                    </div>
                    <div>
                        {props.dataSeat.lsSeat1.rows &&
                            props.dataSeat.lsSeat1.rows.map((row, i) => (
                                <div key={i} style={{ display: 'flex', gap: '5px', marginBottom: '5px', justifyContent: 'center' }}>
                                <span style={{ width: '20px', visibility: row.seats.length === 0 ? 'hidden' : 'visible', color: 'black' }}>
                                    {row.physicalName}
                                </span>
                                <p><SeatCoupleButton seatPairs={seatPairs} seatName={row.physicalName} seatCodeSelect={handleSeatCode}/></p>
                                {/* {Array(props.dataSeat.lsSeat1.width).fill(0).map((_, i2) => {
                                const isShow = row.seats.some(s => s.position.rowIndex === i && s.position.columnIndex === i2);
                                const seatCode = `${row.physicalName}${i2}`;
                                return <SeatButton key={i2} seatCode={seatCode} isShow={isShow} seatCodeSelect={handleSeatCode} />;
                                })} */}
                                </div>
                            ))}
                    </div>
                    <div style={{display:'flex', justifyContent:'center', alignContent:'center',padding:'10px 5px'}}>
                      <div><span>Ghế đơn:</span><span style={{ width: '20px', backgroundColor:'#009d96', color: 'white', padding:'5px', textAlign:'center', boxSizing:'border-box'   }}>A1</span></div>
                      <div><span>Ghế đôi:</span><span style={{ width: '40px', backgroundColor:'#009d96', color: 'white', padding:'5px', textAlign:'center', boxSizing:'border-box'   }}>P5 | P6</span></div>
                    </div>
                </div>
            </div>
            
            <div className='infoTicket'>
                <div>
                    <img src={ticket.imgMovie} />
                </div>

                <div>
                    <h2 className='name'>{ticket.nameMovie}</h2>
                </div>

                <div>
                    <p><span>Rạp: </span>{ticket.CinemaName}</p>
                    <p><span>Screen:</span>{ticket.ScreenName}</p>
                    <p><span>Suất chiếu: </span>{ticket.Showtime} | {ticket.DateShow}, {ticket.DayOfWeek}</p>
                    <p><span>Số lượng vé: </span>{ticket.ticketCount}</p>
<<<<<<< HEAD
                    <p><li>Ghế đơn: {ticket.ticketSingle}</li></p>
                    <p><li>Ghế đôi: {ticket.ticketCouple}</li></p>
=======
>>>>>>> 208927756f3beadbbc0631332f596f5f633bfada
                    <p><span>Mã ghế:</span>
                    {Object.values(selectedSeats).join(", ")}
                    </p>
                    <p><span>Combo:</span>{ticket.comboName}</p>
                    <p><span>Tổng: </span>{ticket.totalSum.toLocaleString()}</p>
                </div>
                <div className='bnt'>
                    <button onClick={() => checkOrder(ticket.ticketSingle,ticket.ticketCouple)}>Tiếp tục</button>
                </div>
                {showPopup && <Popup noti="Vui lòng chọn đúng số lượng ghế" onClose={handleClosePopup} />}
            </div> 
            
        </div>
    </div>
  )
}

const mapStateToProps = (globalState) => {
    return {
        dataSeat: globalState.seatManage
    }
  }
  const mapDispatchToProps = (dispath) => {
    return {
        GetSeat: () => {
            dispath({
                type: actSeat.GET_SEAT
            })
        },
        GetSold:() => {
          dispath({
            type: actSeat.GET_SOLD_SEAT
          })
        }
  
       
    }
  }
  export default connect(mapStateToProps, mapDispatchToProps)(SelectionSeat);