import React, { useEffect } from 'react'
import actSeat from '../../../Redux/actionCenima/actSeat'
import { connect } from 'react-redux'

function SeatSold(props) {

    useEffect(()=>{
        props.GetSold()
    }, [])
  return (
    <div>
        {
            props.dataSeat.lsSold.map((n,i)=>{
                const seatCodesArray = n.SeatCode.split(',');

                return <div key={i}>
                {seatCodesArray.map((seatCode, index) => (
                <p key={index}>{seatCode}</p>
                ))}
                </div>
            })
        }
    </div>
  )
}


const mapStateToProps = (globalState) => {
    return {
        dataSeat: globalState.seatManage
    }
  }
  const mapDispatchToProps = (dispath) => {
    return {
        GetSold:() => {
          dispath({
            type: actSeat.GET_SOLD_SEAT
          })
        }
    }
  }
  export default connect(mapStateToProps, mapDispatchToProps)(SeatSold);