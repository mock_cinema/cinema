import React, { useState } from 'react'

export default function SeatButton({ seatCode, isShow, seatCodeSelect, dataSoldSeat  }) {
    const [isHovered, setIsHovered] = useState(false);
    const [isChecked, setIsChecked] = useState(false);
    const [isSelected, setIsSelected] = useState(false);

    const handleMouseEnter = () => {
      setIsHovered(true);
    };
  
    const handleMouseLeave = () => {
      setIsHovered(false);
    };



  
    // const handleClick = () => {
    //   setIsChecked((prevIsChecked) => !prevIsChecked);

    //   const selectedSeats = JSON.parse(sessionStorage.getItem("selectedSeats")) || [];

    //   if (isSelected) {
    //     // Seat is already selected, remove it from the session
    //     const updatedSeats = selectedSeats.filter((code) => code !== seatCode);
    //     sessionStorage.setItem("selectedSeats", JSON.stringify(updatedSeats));
    //     seatCodeSelect(updatedSeats)
    //     setIsSelected(false);
        
    //   } else {
    //     // Seat is not selected, add it to the session
    //     const updatedSeats = [...selectedSeats, seatCode];
    //     sessionStorage.setItem("selectedSeats", JSON.stringify(updatedSeats));
    //     seatCodeSelect(updatedSeats)
    //     setIsSelected(true);
    //   }
      
      
    // };

    // const handleClick = () => {
    //   setIsChecked((prevIsChecked) => !prevIsChecked);
  
    //   const selectedSeats = JSON.parse(sessionStorage.getItem("selectedSeats")) || [];
    //   const selectedSeatsData = JSON.parse(sessionStorage.getItem("selectedSeatsData")) || { mode: 1, seats: [] };
  
    //   if (isSelected) {
    //     // Seat is already selected, remove it from the session
    //     const updatedSeats = selectedSeats.filter((code) => code !== seatCode);
    //     sessionStorage.setItem("selectedSeats", JSON.stringify(updatedSeats));
    //     seatCodeSelect(updatedSeats);
    //     setIsSelected(false);
  
    //     // Remove the seat data from selectedSeatsData object
    //     const updatedSeatsData = {
    //       ...selectedSeatsData,
    //       seats: selectedSeatsData.seats.filter((seatData) => seatData[seatCode] !== true),
    //     };
    //     sessionStorage.setItem("selectedSeatsData", JSON.stringify(updatedSeatsData));
    //   } else {
    //     // Seat is not selected, add it to the session
    //     const updatedSeats = [...selectedSeats, { mode: 1, seatCodes: seatCode }];
    //     sessionStorage.setItem("selectedSeats", JSON.stringify(updatedSeats));
    //     seatCodeSelect(updatedSeats);
    //     setIsSelected(true);
        
    //     // Add the seat data to selectedSeatsData object with mode and seatCode properties
    //     // const updatedSeatsData = {
    //     //   ...selectedSeatsData,
    //     //   seats: [...selectedSeatsData.seats, { mode: 1, seatCodes:seatCode }],
    //     // };
    //     // sessionStorage.setItem("selectedSeatsData", JSON.stringify(updatedSeatsData));
    //   }
    // };

    const handleClick = () => {
      setIsChecked((prevIsChecked) => !prevIsChecked);
    
      const selectedSeats = JSON.parse(sessionStorage.getItem("selectedSeats")) || [];
      const selectedSeatsData = JSON.parse(sessionStorage.getItem("selectedSeatsData")) || { mode: 1, seats: [] };
    
      if (isSelected) {
        // Seat is already selected, remove it from the session
        const updatedSeats = selectedSeats.filter((code) => code !== seatCode);
        sessionStorage.setItem("selectedSeats", JSON.stringify(updatedSeats));
        seatCodeSelect(updatedSeats);
        setIsSelected(false);
    
        // Remove the seat data from selectedSeatsData object
        const updatedSeatsData = {
          ...selectedSeatsData,
          seats: selectedSeatsData.seats.filter((seatData) => seatData.seatCodes !== seatCode),
        };
        sessionStorage.setItem("selectedSeatsData", JSON.stringify(updatedSeatsData));
      } else {
        // Seat is not selected, add it to the session
        const updatedSeats = [...selectedSeats, seatCode];
        sessionStorage.setItem("selectedSeats", JSON.stringify(updatedSeats));
        seatCodeSelect(updatedSeats);
        setIsSelected(true);
    
        // Check if the seat data already exists in selectedSeatsData
        const existingSeatData = selectedSeatsData.seats.find((seatData) => seatData.seatCodes === seatCode);
    
        if (existingSeatData) {
          // Seat data already exists, update the mode to 1
          const updatedSeatsData = {
            ...selectedSeatsData,
            seats: selectedSeatsData.seats.map((seatData) =>
              seatData.seatCodes === seatCode ? { ...seatData, mode: 1 } : seatData
            ),
          };
          sessionStorage.setItem("selectedSeatsData", JSON.stringify(updatedSeatsData));
        } else {
          // Seat data does not exist, add it with mode 1
          const updatedSeatsData = {
            ...selectedSeatsData,
            seats: [...selectedSeatsData.seats, { mode: 1, seatCodes: seatCode }],
          };
          sessionStorage.setItem("selectedSeatsData", JSON.stringify(updatedSeatsData));
        }
      }
    };
    
  
    const getBackgroundColor = () => {
      if (isChecked) {
        return 'blue'; // Change to your desired checked color
      } else if (isHovered) {
        return 'red'; // Change to your desired hover color
      } else {
        return '#009d96'; // Change to your default color
      }
    };
  
    return (
      <button
        style={{
          backgroundColor: getBackgroundColor(),
          color: 'white',
          width: '25px',
          visibility: isShow ? '' : 'hidden',
          textAlign: 'center',
          fontSize:'11px',
          
        }}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        onClick={handleClick}
      
      >
        {seatCode}
      </button>
    );
}
