import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import actCenima from '../../Redux/actionCenima/actCenima'
import "./Ticket.scss"
import { useNavigate } from 'react-router-dom'

function Ticket(props) {

  const [infoDates, setInfoDates] = useState("")
  const [infoDays, setInfoDays] = useState("")


  const nav = useNavigate()
  useEffect(() => {
    props.GetBooking()
  }, [])

  const movie = props.dataCenima.lsBooking.movies
  //const MovieDetail = props.dataCenima.lsBooking.movies?.find(n => n.id == ticket.idMovie)

  const HandleId = (id, name, image) => {
    //sessionStorage.setItem("idMovieTicket", id)
    const Ticket = {
      idMovie:id,
      nameMovie:name,
      imgMovie:image
    }
    
    sessionStorage.setItem("Ticket", JSON.stringify(Ticket))
    props.MovieIdTicket(id)
  }

  const HandleIdDates = (idCenima, nameCenima) => {
    const ticket = JSON.parse(sessionStorage.getItem("Ticket")) ;
    const Ticket = {
      ...ticket,
      idCinema: idCenima,
      CinemaName: nameCenima
    }
    sessionStorage.setItem("Ticket", JSON.stringify(Ticket))
    props.MovieIdDates(idCenima)
  }

  const SelectionTicket = (showTime, id, screenName, screenNumber, dateWeek, dateShow) => {
    const ticket = JSON.parse(sessionStorage.getItem("Ticket")) ;
    const Ticket = {
        ...ticket,
        Showtime: showTime,
        SessionId: id,
        ScreenName: screenName,
        ScreenNumber: screenNumber,
        DayOfWeek: dateWeek,
        DateShow: dateShow,
      };

    sessionStorage.setItem('Ticket', JSON.stringify(Ticket));

    nav('/selectionticket')
}
  return (
    <div className='Ticket'>
      <div className='Container'>
        <div className='SelectionMovie'>
          <table>
            <thead>
              <tr>
                <th>CHỌN PHIM</th>
              </tr>
            </thead>
            <tbody>
              {
                movie?.map((n, i) => {
                  let id = n.id
                  return (
                    <tr key={i} >
                      <td onClick={() => HandleId(id,n.name,n.imageLandscape)}>
                        <div className='im'>
                          <img src={n.imageLandscape} />
                        </div>
                        <div className='content'>
                          <p className='p1'>{n.name}</p>
                          <p>{n.subName}</p>
                        </div>
                      </td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
        </div>
        <div className='SelectionCenima'>
          <table>
            <thead>
              <tr>
                <th>CHỌN RẠP</th>
              </tr>
            </thead>
            <tbody>
              {
                props.dataCenima.lsMovieId?.map((n, i) => {
                  let idCenima = n.id
                  let nameCenima = n.name
                  
                  return (
                    <tr key={i}>
                      <td onClick={() => { HandleIdDates(idCenima, nameCenima) }}>{n.name}</td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
        </div>
        <div className='SeclectionDates'>
          <table>
            <thead>
              <tr>
                <th>CHỌN SUẤT CHIẾU</th>
              </tr>
            </thead>
            <tbody>
              {
                props.dataCenima.lsDates.dates?.map((n, i) => {
                  let sub = n.bundles.find(n => n.caption == "sub")
                  let longtieng = n.bundles.find(n => n.caption == "voice")
                  let date = n.showDate
                  let days = n.dayOfWeekLabel

                  return (
                    <tr key={i}>
                      <td>
                        <h2 style={{ color: "salmon", fontWeight: "bold" }}>{n.dayOfWeekLabel} {n.showDate}</h2>
                        <div className='phude'>
                          <p>{sub?.version ? "2D" : ""} {sub?.caption ? "Phụ đề" : ""}</p>
                          <div className='giochieu'>
                            {sub?.sessions.map((n3, i3) => {
                              let showtime = n3.showTime
                              return <button onClick={() => { SelectionTicket(n3.showTime,n3.id,n3.screenName,n3.screenNumber, n3.showDate, n3.dayOfWeekLabel) }} key={i3}>{n3.showTime}</button>
                            })}
                          </div>
                        </div>
                        <div className='longtieng'>
                          <p>{longtieng?.version ? "2D" : ""} {longtieng?.caption ? "L.Tiếng" : ""}</p>
                          <div className='giochieu'>
                            {longtieng?.sessions.map((n3, i3) => {
                              let showtime = n3.showTime
                              return <button onClick={() => { SelectionTicket(n3.showTime,n3.id,n3.screenName,n3.screenNumber, n3.showDate, n3.dayOfWeekLabel) }} key={i3}>{n3.showTime}</button>
                            })}
                          </div>
                        </div>
                      </td>
                    </tr>
                  )
                })
              }

            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}
const mapStateToProps = (globalState) => {
  return {
    dataCenima: globalState.dataManage
  }
}
const mapDispatchToProps = (dispath) => {
  return {
    GetBooking: () => {
      dispath({
        type: actCenima.GET_BOOKING
      })
    },
    MovieIdTicket: (idMovie) => {
      dispath({
        type: actCenima.GET_MOVIE_ID,
        payload: idMovie
      })
    },
    MovieIdDates: (idCenima) => {
      dispath({
        type: actCenima.SET_MOVIE_DATES,
        payload: idCenima
      })
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Ticket);

