import React, { useEffect } from 'react';

const TicketPurchaseComponent = () => {
  // Assuming you have the ticket object passed as a prop to this component
  const ticket = JSON.parse(sessionStorage.getItem("Ticket"));

  const showtimeString = ticket.Showtime; // Replace this with your actual showtime string
const dayOfWeekString = ticket.DayOfWeek; // Replace this with your actual DayOfWeek string

// Parse the DayOfWeek string to get the day, month, and year
const [day, month, year] = dayOfWeekString.split('/').map(Number);

// Split the showtime string into hours and minutes
const [hours, minutes] = showtimeString.split(':').map(Number);

// Create a new Date object with the extracted date and time
const showtimeDate = new Date(Date.UTC(year, month - 1, day, hours, minutes));

// Format the date to ISO 8601 format with 'Z' indicating UTC timezone
const formattedShowtime = showtimeDate.toISOString();

console.log(formattedShowtime); // Output: "2023-08-02T10:00:00.000Z"



  const payTicket = {
    BankId: 1,
    CardNumber: "4946344985754473",
    CardName: "Nguyen Kim An",
    ExpireDate: "0628",
    CVV: "146",
    Price: ticket.totalSum,
    ShowCode: ticket.SessionId,
    Email: "annk.sale@gmail.com",
    CinemaName: ticket.CinemaName,
    TheaterName: ticket.ScreenName,
    FilmName: ticket.MovieName,
    ImageLandscape: ticket.imgMovie,
    ImagePortrait: ticket.imgMovie,
    Combo: ticket.comboName,
    SeatCode: ticket.seatCode,
    ShowTime: "2023-01-13T20:30Z",
}
//console.log(payTicket.CardNumber)
  // Function to send the payTicket object to the API
  const sendTicketToAPI = async () => {
    try {
      const response = await fetch('https://teachingserver.org/U2FsdGVkX18MaY1VB6bVfvVBm0wdPflO/cinema/Ticket', {
        method: 'POST',
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(payTicket),
      });
      //console.log(JSON.stringify(payTicket))
      if (response.ok) {
        // Handle successful response here
        console.log('Ticket purchase successful!');
      } else {
        // Handle error response here
        console.error('Ticket purchase failed.');
      }
    } catch (error) {
      console.error('An error occurred while sending the ticket:', error);
    }
  };

  useEffect(() => {
    // Call the function to send the ticket to the API when the component mounts
    sendTicketToAPI();
  }, []);

  // Rest of the component JSX goes here...
  return <div>...</div>;
};

export default TicketPurchaseComponent;
