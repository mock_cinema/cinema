import "./Home.scss"
import React, { useEffect, useState } from 'react'
import { connect } from "react-redux"
import { Link, useNavigate } from 'react-router-dom'
import actCenima from "../../Redux/actionCenima/actCenima"
import SlideShow from "../SlideShow/SlideShow"
import Loading from "../Loading/Loading"

function Home(props) {
  const nav = useNavigate()
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    props.GetDataCenima()
<<<<<<< HEAD
    setIsLoading(false);
    sessionStorage.clear();
  }, 5000)
=======
    sessionStorage.clear();
  }, [])
>>>>>>> 208927756f3beadbbc0631332f596f5f633bfada
  
  const lsMovieNow = props.dataCenima.lsDataCenima.movieShowing

  const GetMovieId = (id, name, image) => {
    const ticket = {
      idMovie:id,
      nameMovie:name,
      imgMovie:image
    }
    sessionStorage.setItem('Ticket', JSON.stringify(ticket));

    nav("/movieticketid")
  }

  return (
    <>
     <div className='Home'>
    <SlideShow />
    <div className='tab'>
      <div className='tab1'>
        <Link to={"/"} style={{ color: "salmon" }}>Phim Đang Chiếu</Link>
      </div>
      <div className='tab2'>
        <Link to={"/moviesoon"}>Phim Sắp Chiếu</Link>
      </div>
    </div>

      {isLoading ? (
        <Loading />
      ) : (
      <div className='Container'>
        {
          lsMovieNow?.map((n, i) => {
            return (
              <div key={i} className='mainCarMovie'>
                <div>
                  <img src={n.imagePortrait} />
                  <h3>{n.name}</h3>
                  <p>{n.subName}</p>
                  <button value={n.id} onClick={() => { GetMovieId(n.id, n.name, n.imageLandscape) }}>MUA VÉ</button>
                </div>
              </div>
            )
          })
        }
      </div>
      )}
    </div>
    </>
  )
}
const mapStateToProps = (globalState) => {
  return {
    dataCenima: globalState.dataManage
  }
}
const mapDispatchToProps = (dispath) => {
  return {
    GetDataCenima: () => {
      dispath({
        type: actCenima.GET_DATA_CENIMA
      })
    },
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);
