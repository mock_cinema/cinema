import React, { useState } from 'react'
import './Popup.scss'
export default function Popupss({noti,onClose }) {
    
 return (
  <div className='Popup'>
  <div className="popup-container">
    <div className="popup">
      <div className="content">
      <button
        className="close-button"
        onClick={onClose}
      >
        &times;
      </button>
        <p>{noti}</p>
      </div>
    </div>
  </div>
</div>
  )
}
